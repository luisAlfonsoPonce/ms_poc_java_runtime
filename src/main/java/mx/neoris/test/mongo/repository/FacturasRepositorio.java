package mx.neoris.test.mongo.repository;

import mx.neoris.test.mongo.model.RptFacturas;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by e-obautista on 21/05/2019.
 */
public interface FacturasRepositorio extends CrudRepository<RptFacturas, Long> {
    List<RptFacturas> findAll();
}
