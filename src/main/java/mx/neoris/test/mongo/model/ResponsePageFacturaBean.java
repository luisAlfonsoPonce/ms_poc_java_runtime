package mx.neoris.test.mongo.model;

import mx.neoris.test.mongo.utils.paginador.PageRender;
import org.springframework.data.domain.Page;

import java.io.Serializable;

/**
 * Clase bean para respuesta
 * de listado de factura
 *
 * @author neoris
 * @version 1.0.0
 */
public class ResponsePageFacturaBean implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 6130478493043141191L;

    private Page<FacturaBean> facturaPaginada;
    private PageRender<FacturaBean> page;
    /**
     * @return the facturaPaginada
     */
    public Page<FacturaBean> getFacturaPaginada() {
        return facturaPaginada;
    }
    /**
     * @param facturaPaginada the facturaPaginada to set
     */
    public void setFacturaPaginada(Page<FacturaBean> facturaPaginada) {
        this.facturaPaginada = facturaPaginada;
    }
    /**
     * @return the page
     */
    public PageRender<FacturaBean> getPage() {
        return page;
    }
    /**
     * @param page the page to set
     */
    public void setPage(PageRender<FacturaBean> page) {
        this.page = page;
    }


}
