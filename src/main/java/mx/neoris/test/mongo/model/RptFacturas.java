package mx.neoris.test.mongo.model;

import java.io.Serializable;

/**
 * Created by e-obautista on 21/05/2019.
 */
public class RptFacturas implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -1927413845777670650L;
    private String idPdfTxt;
    private String nombrePdfTxt;
    /**
     * @return the idPdfTxt
     */
    public String getIdPdfTxt() {
        return idPdfTxt;
    }
    /**
     * @param idPdfTxt the idPdfTxt to set
     */
    public void setIdPdfTxt(String idPdfTxt) {
        this.idPdfTxt = idPdfTxt;
    }
    /**
     * @return the nombrePdfTxt
     */
    public String getNombrePdfTxt() {
        return nombrePdfTxt;
    }
    /**
     * @param nombrePdfTxt the nombrePdfTxt to set
     */
    public void setNombrePdfTxt(String nombrePdfTxt) {
        this.nombrePdfTxt = nombrePdfTxt;
    }


}
