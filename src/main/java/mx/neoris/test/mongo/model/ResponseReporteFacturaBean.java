package mx.neoris.test.mongo.model;

/**
 * Clase bean para respuesta
 * de reporte de factura
 *
 * @author neoris
 * @version 1.0.0
 */
public class ResponseReporteFacturaBean {
    private byte[] pdfFile;

    /**
     * @return the pdfFile
     */
    public byte[] getPdfFile() {
        byte[] pdfFileTmp = new byte[0];
        if (this.pdfFile != null) {
            pdfFileTmp = this.pdfFile;
        }
        return pdfFileTmp;
    }


    /**
     * @param pdfFile the pdfFile
     *                to set
     */
    public void setPdfFile(byte[] pdfFile) {
        byte[] pdfFileTmp = new byte[0];
        if (pdfFile != null) {
            pdfFileTmp = pdfFile;
        }
        this.pdfFile = pdfFileTmp;
    }
}
