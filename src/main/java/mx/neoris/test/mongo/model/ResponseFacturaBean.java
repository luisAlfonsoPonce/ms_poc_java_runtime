package mx.neoris.test.mongo.model;

import java.io.Serializable;

/**
 * Clase bean para respuesta
 * de listado de factura
 *
 * @author neoris
 * @version 1.0.0
 */
public class ResponseFacturaBean implements Serializable {

    /**
     * Serial
     */
    private static final long serialVersionUID = 8991440324532854831L;

    private FacturaBean facturaBean;

    /**
     * @return the facturaBean
     */
    public FacturaBean getFacturaBean() {
        return facturaBean;
    }

    /**
     * @param facturaBean the facturaBean to set
     */
    public void setFacturaBean(FacturaBean facturaBean) {
        this.facturaBean = facturaBean;
    }
}
