package mx.neoris.test.mongo.utils.paginador;

import org.springframework.data.domain.Page;

import java.util.ArrayList;
import java.util.List;

/**
 * Clase paga paginado
 * que hace el calculo de
 * numeroElementosPorPagina
 * totalPaginas
 * paginaActual
 *
 * @param <T> PageRender de cualquier clase de Bean
 * @author neoris
 * @version 1.0.0
 */
public class PageRender<T> {

    private String url;
    private Page<T> page;

    private int totalPaginas;
    private int numeroElementosPorPagina;
    private int paginaActual;
    private List<PageItem> paginas;

    /**
     * Constructor que hace el calculo de
     * numeroElementosPorPagina
     * totalPaginas
     * paginaActual
     *
     * @param url  en caso de volver a llamar a back
     * @param page objeto de paginado con total paginas
     */
    public PageRender(String url, Page<T> page) {
        this.url = url;
        this.page = page;
        this.paginas = new ArrayList<>();
        int desde, hasta;

        numeroElementosPorPagina = page.getSize();
        totalPaginas = page.getTotalPages();
        paginaActual = page.getNumber() + 1;

        if (totalPaginas <= numeroElementosPorPagina) {
            desde = 1;
            hasta = totalPaginas;
        } else {
            // validacion de primer pagina,
            // actual pagina o
            // ultima pagina
            if (paginaActual <= numeroElementosPorPagina / 2) {
                desde = 1;
                hasta = numeroElementosPorPagina;
            } else if (paginaActual >= totalPaginas - numeroElementosPorPagina / 2) {
                desde = totalPaginas - numeroElementosPorPagina + 1;
                hasta = numeroElementosPorPagina;
            } else {
                desde = paginaActual - numeroElementosPorPagina / 2;
                hasta = numeroElementosPorPagina;
            }
        }
        for (int i = 0; i < hasta; i++) {
            paginas.add(new PageItem(desde + i, paginaActual == desde + i));
        }
    }

    /**
     * @return the url
     */
    public String getUrl() {
        return url;
    }

    /**
     * @param url the url
     *            to set
     */
    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * @return the paginas
     */
    public List<PageItem> getPaginas() {
        List<PageItem> paginastmp = new ArrayList<>();
        if (this.paginas != null) {
            paginastmp.addAll(this.paginas);
        }
        return paginastmp;
    }

    /**
     * @return int totalPaginas
     */
    public int getTotalPaginas() {
        return totalPaginas;
    }

    /**
     * @return int paginActual
     */
    public int getPaginaActual() {
        return paginaActual;
    }

    /**
     * si es primer pagina
     * @return boolean
     */
    public boolean isFirst() {
        return page.isFirst();
    }

    /**
     * si es ultima pagina
     * @return boolean
     */
    public boolean isLast() {
        return page.isLast();
    }

    public boolean isHasNext() {
        return page.hasNext();
    }

    public boolean isHasPrevious() {
        return page.hasPrevious();
    }
}
