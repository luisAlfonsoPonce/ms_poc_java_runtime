package mx.neoris.test.mongo.utils.paginador;

/**
 * Clase model paga paginacion
 *
 * @author neoris
 * @version 1.0.0
 */
public class PageItem {

    private int numero;
    private boolean actual;

    /**
     * Constructor
     * @param numero pagina indicada por usuario
     * @param actual pagina actual desde Bd
     */
    public PageItem(int numero, boolean actual) {
        this.numero = numero;
        this.actual = actual;
    }

    /**
     * @return the numero
     */
    public int getNumero() {
        return numero;
    }

    /**
     * @param numero the numero to set
     */
    public void setNumero(int numero) {
        this.numero = numero;
    }

    /**
     * @return the actual
     */
    public boolean isActual() {
        return actual;
    }

    /**
     * @param actual the actual to set
     */
    public void setActual(boolean actual) {
        this.actual = actual;
    }
}
