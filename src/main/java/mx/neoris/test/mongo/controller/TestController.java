package mx.neoris.test.mongo.controller;

import mx.neoris.test.mongo.model.RptFacturas;
import mx.neoris.test.mongo.repository.FacturasRepositorio;
import mx.neoris.test.mongo.utils.generatezip.UtilGeneratorZip;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipOutputStream;

@RestController
@CrossOrigin(allowCredentials = "true", origins = "*")
@RequestMapping("/")
public class TestController {

    @Autowired
    private FacturasRepositorio facturasRepository;

    @RequestMapping(value = "/consulta", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    //cuando es GET es @RequestParam
    public ResponseEntity<List<RptFacturas>> testMongo() {
        List<RptFacturas> r = new ArrayList<>();
        r = facturasRepository.findAll();
        return new ResponseEntity<>(r, HttpStatus.OK);
    }
    @RequestMapping(value = "/download" , method=RequestMethod.GET , produces = "application/zip")
    public ResponseEntity<InputStreamResource> downloadPdf() throws IOException {
        String sourceFile = "D:\\Respaldo\\Proyectos\\PoC\\mv-pdf-cfdi-service\\src\\main\\resources\\zipTemp\\1234567890.pdf";
        String sourceFile1 = "D:\\Respaldo\\Proyectos\\PoC\\mv-pdf-cfdi-service\\src\\main\\resources\\zipTemp\\2345678901.pdf";
        FileOutputStream fos = new FileOutputStream("D:\\Respaldo\\Proyectos\\PoC\\mv-pdf-cfdi-service\\src\\main\\resources\\reportPDF.zip");
        ZipOutputStream zipOut = new ZipOutputStream(fos);
        File fileToZip = new File(sourceFile);
        File fileToZip1 = new File(sourceFile1);

        UtilGeneratorZip.zipFile(fileToZip, fileToZip.getName(), zipOut);
        UtilGeneratorZip.zipFile(fileToZip1, fileToZip1.getName(), zipOut);
        zipOut.close();
        fos.close();
        HttpHeaders headers = new HttpHeaders();
        ClassPathResource pdfFile = new ClassPathResource("reportPDF.zip");
        headers.setContentType(MediaType.parseMediaType(MediaType.APPLICATION_OCTET_STREAM_VALUE));
        headers.add("Access-Control-Allow-Origin", "*");
        headers.add("Access-Control-Allow-Methods", "GET, POST, PUT");
        headers.add("Access-Control-Allow-Headers", "Content-Type");
        headers.add("Content-Disposition", "attachment;filename=" + "reportPDF.zip");
        headers.add("Cache-Control", "no-cache, no-store, must-revalidate");
        headers.add("Pragma", "no-cache");
        headers.add("Expires", "0");
        headers.setContentLength(pdfFile.contentLength());
        return new ResponseEntity<>(new InputStreamResource(
                pdfFile.getInputStream()), headers, HttpStatus.OK);
    }
    @RequestMapping(value = "/insert"
            , method = RequestMethod.PUT
            , consumes = MediaType.APPLICATION_JSON_VALUE
            , produces = MediaType.APPLICATION_JSON_VALUE)
    //cuando es GET es @RequestParam
    public ResponseEntity<RptFacturas> insert(@RequestBody RptFacturas facturaNueva) {
        RptFacturas g = facturasRepository.save(facturaNueva);
        return new ResponseEntity<>(g, HttpStatus.OK);
    }

   /**
     * metodo para pdf
    *
    * @param id identificador
    * @return ResponseEntity singleResponse
    */
    @RequestMapping(value = "/getPDF", method = RequestMethod.GET , produces = MediaType.APPLICATION_JSON_VALUE)
    //cuando es GET es @RequestParam
    public ResponseEntity<String> getPDF(@RequestParam ("idPDF") String id) {
        String singleResponse = "{\n" +
                "  \"responseCode\": {\n" +
                "    \"code\": \"CAJE000\",\n" +
                "    \"message\": \"Operación realizada correctamente\",\n" +
                "    \"level\": \"SUCCESS\",\n" +
                "    \"description\": \"Describe que una operación se efectuó correctamente\",\n" +
                "    \"moreInfo\": \"http://mesa.ayuda.com/7\"\n" +
                "  },\n" +
                "  \"response\": {\n" +
                "    \"id\" : \""+id+"\",\n" +
                "    \"url\" : \"C:\\Users\\E-OBAUTISTA\\Documents\\RV_flujoCOMPL_v5.pdf\"\n" +
                "  },\n" +
                "  \"ok\": true,\n" +
                "  \"validations\": []\n" +
                "}";
        return new ResponseEntity<>(singleResponse, HttpStatus.OK);
    }

    /**
     * Metodo prueba para probar conexión con mongoDb
     *
     * @return response con el conteo de registros en la coleccion Persona
     */
    @RequestMapping(value = "/", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> consultaInfo() {
        String singleResponse = "{\n" +
                "  \"responseCode\": {\n" +
                "    \"code\": \"CAJE000\",\n" +
                "    \"message\": \"Operación realizada correctamente\",\n" +
                "    \"level\": \"SUCCESS\",\n" +
                "    \"description\": \"Describe que una operación se efectuó correctamente\",\n" +
                "    \"moreInfo\": \"http://mesa.ayuda.com/7\"\n" +
                "  },\n" +
                "  \"response\": [\n" +
                "    {\n" +
                "      \"ide_id\": \"001\",\n" +
                "      \"cve_fol_interno\": \"528777\",\n" +
                "      \"cve_fol_sat\": \"85258C91-B4D5-4699-BD10-921D109A3861\",\n" +
                "      \"tpo_documento\": \"0\",\n" +
                "      \"bnd_estatus\": \"VIGENTE\",\n" +
                "      \"txt_rfc_emisor\": \"PENL8510108FG\",\n" +
                "      \"txt_rfc_receptor\": \"LUHB2020106K9\",\n" +
                "      \"fec_cancelar\": \"2019-05-01\",\n" +
                "      \"fec_emision\": \"2019-05-01\",\n" +
                "      \"mto_subtotal\": \"1000.00\",\n" +
                "      \"imp_iva\": \"160.00\",\n" +
                "      \"mto_total\": \"1160.00\"\n" +
                "    },\n" +
                "    {\n" +
                "      \"ide_id\": \"002\",\n" +
                "      \"cve_fol_interno\": \"528779\",\n" +
                "      \"cve_fol_sat\": \"85258C91-B4D5-4699-BD10-921D109A3861\",\n" +
                "      \"tpo_documento\": \"0\",\n" +
                "      \"bnd_estatus\": \"VIGENTE\",\n" +
                "      \"txt_rfc_emisor\": \"PENL8510108FG\",\n" +
                "      \"txt_rfc_receptor\": \"LUHB2020106K9\",\n" +
                "      \"fec_cancelar\": \"2019-05-01\",\n" +
                "      \"fec_emision\": \"2019-05-01\",\n" +
                "      \"mto_subtotal\": \"1000.00\",\n" +
                "      \"imp_iva\": \"160.00\",\n" +
                "      \"mto_total\": \"1160.00\"\n" +
                "    }\n" +
                "  ],\n" +
                "  \"ok\": true,\n" +
                "  \"validations\": []\n" +
                "}";
        return new ResponseEntity<>(singleResponse, HttpStatus.OK);
    }
}
